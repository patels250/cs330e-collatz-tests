#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "60532 89088\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  60532)
        self.assertEqual(j, 89088)

    def test_read_3(self):
        s = "73 542\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  73)
        self.assertEqual(j, 542)

    def test_read_4(self):
        s = "125 723\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  125)
        self.assertEqual(j, 723)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(86, 99)
        self.assertEqual(v, 119)

    def test_eval_6(self):
        v = collatz_eval(178, 888)
        self.assertEqual(v, 179)

    def test_eval_7(self):
        v = collatz_eval(5, 12)
        self.assertEqual(v, 20)

    def test_eval_8(self):
        v = collatz_eval(9797, 66566)
        self.assertEqual(v, 340)

    def test_eval_9(self):
        v = collatz_eval(4, 33078)
        self.assertEqual(v, 308)

    def test_eval_10(self):
        v = collatz_eval(953, 1021)
        self.assertEqual(v, 156)

    def test_eval_11(self):
        v = collatz_eval(844, 7789)
        self.assertEqual(v, 262)

    def test_eval_12(self):
        v = collatz_eval(90, 732)
        self.assertEqual(v, 171)

    def test_eval_13(self):
        v = collatz_eval(4555, 8765)
        self.assertEqual(v, 262)

    def test_eval_14(self):
        v = collatz_eval(40071, 82911)
        self.assertEqual(v, 351)

    def test_eval_15(self):
        v = collatz_eval(66854, 32812)
        self.assertEqual(v, 340)

    def test_eval_16(self):
        v = collatz_eval(957, 3845)
        self.assertEqual(v, 238)

    def test_eval_17(self):
        v = collatz_eval(562, 745)
        self.assertEqual(v, 171)

    def test_eval_18(self):
        v = collatz_eval(55, 99)
        self.assertEqual(v, 119)

    def test_eval_19(self):
        v = collatz_eval(8, 20)
        self.assertEqual(v, 21)

    def test_eval_20(self):
        v = collatz_eval(55289, 78003)
        self.assertEqual(v, 351)

    def test_eval_21(self):
        v = collatz_eval(7, 318931)
        self.assertEqual(v, 443)

    def test_eval_22(self):
        v = collatz_eval(988543, 7897)
        self.assertEqual(v, 525)

    def test_eval_23(self):
        v = collatz_eval(483,43180)
        self.assertEqual(v, 324)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 963, 1820, 182)
        self.assertEqual(w.getvalue(), "963 1820 182\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 88, 765, 171)
        self.assertEqual(w.getvalue(), "88 765 171\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 47, 48, 105)
        self.assertEqual(w.getvalue(), "47 48 105\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 1\n3 99999\n1 233\n4 17\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n3 99999 351\n1 233 128\n4 17 20\n")

    def test_solve_3(self):
        r = StringIO("5200 7600\n88 99322\n90110 101010\n8 20\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5200 7600 262\n88 99322 351\n90110 101010 341\n8 20 21\n")

    def test_solve_4(self):
        r = StringIO("90 732\n178 888\n867 4429\n90 95\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "90 732 171\n178 888 179\n867 4429 238\n90 95 106\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
